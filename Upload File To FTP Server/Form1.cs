﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// using

using System.IO;
using System.Net;

namespace Upload_File_To_FTP_Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // making a upload method
            string filepath = @"C:\Users\Md. Alamin Mahamud\Desktop\my log.txt";
            Upload(filepath);
        }

        public void Upload(string filePath)
        {
            // try to create a connection
            try
            {
                string ftpUri = @"ftp://ineedahelp.com/public_html/sadiq_upload/";
                string ftpUsername = "your ftp username";
                string ftppwd = "your ftp password";
                FileInfo fileinfo = new FileInfo(filePath);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpUri+""+fileinfo.Name);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(ftpUsername, ftppwd);

                Stream ftpStream = request.GetRequestStream();
                FileStream file = File.OpenRead(filePath);

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead !=0);

                file.Close();
                ftpStream.Close();
                label1.Text = "Done";
            }
            catch(Exception ex)
            {
                label1.Text = ex.Message;
            }
        }
    }
}
